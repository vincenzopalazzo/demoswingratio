package io.vincentpalazzo.ratio.view;

import javax.swing.*;

public interface IPresentationPanel extends IAppViewComponent{

    JLabel getBackgroundLabel();

    JButton getAddBackground();
}
