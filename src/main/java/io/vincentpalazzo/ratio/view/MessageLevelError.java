package io.vincentpalazzo.ratio.view;

public enum MessageLevelError {

    INFO,
    ERROR,
    WARNING
}
