package io.vincentpalazzo.ratio.view;

/**
 * @author https://github.com/vincenzopalazzo
 */
public interface IMainPanel extends IAppViewComponent{
    //TagInterface

    void refreshUI();
}
