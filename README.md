# DemoSwingRatio

## The demo is moved inside [here](https://github.com/material-ui-swing/DemoSwingRatio)

This demo is a Swing application and I'm trying to implement the ration effect at an panel inside the application (Like Powerpoint)

You can download the jar at this [link](https://gitlab.com/palazzovincenzo/demoswingratio/-/tree/master/bin)

## Results

After the first test with the demo Swing, I can conclude that is possible set the personal ratio at runtime with *complex* panels, like Mars use case [JMars](http://jmars.mars.asu.edu/)

_Waiting other tests_

At the moment the image generate with the demo are:

### 4:9

![](results/images-generated/bane_gotham_4:9.png)

### 1:1

![](results/images-generated/bane_gotham_1:1.png)

### 16:9

![](results/images-generated/bane_gotham_16:9.png)

### Profiler

You can found also the profiler snapshot YourKit[Download](https://gitlab.com/vincenzopalazzo/demoswingratio/-/raw/master/results/profiler-snapshot/RUN-2020-04-04.snapshot?inline=false)

# TODO list

- [X] [Create a method to generate the image from a component](https://stackoverflow.com/questions/1349220/convert-jpanel-to-image)
- [X] [Create a method to load personal image inside the JPanel (like powerpoint)](https://stackoverflow.com/questions/22162398/how-to-set-a-background-picture-in-jpanel)

# Sponsor

If my work was utils to other developers, you can make a little donation at the following link (if you want :))

[![Donation](https://img.shields.io/website/http/vincenzopalazzo.github.io/material-ui-swing-donations.svg?style=for-the-badge&up_color=yellow&up_message=Donation)](https://vincenzopalazzo.github.io/material-ui-swing-donations)

![Custom badge](https://img.shields.io/endpoint?style=for-the-badge&url=https%3A%2F%2Fshieldsio-patreon.herokuapp.com%2Fmaterialuiswing)

[Github donations](https://github.com/sponsors/vincenzopalazzo?preview=true)

[Liberapay](https://liberapay.com/vincenzopalazzo)

<p align="center" style="font-weight: bold;"> This demo is developed in collaborations with Arizona State University. </p>

<div align="center">
<img src="https://sundevilgymnastics.com/wp-content/uploads/2016/10/ASU-Womens-Gymnastics-Website.png" />
</div>

